import json
import requests
import pandas as pd
import time
import os

tags = ['%23PCL90CQQ', '%23PQ2LJ0GPU']
api_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImFmN2E0NDE0LTUwODItNDQyOC1hNjY0LWM0MTU5YTY2OWIzNCIsImlhdCI6MTU4OTY4MTMxOSwic3ViIjoiZGV2ZWxvcGVyL2EyNDYwMDRmLTExN2QtNTA4OS01YzNkLWY4OWI4MGM1YzU2MSIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTc5LjY3LjEuNTYiXSwidHlwZSI6ImNsaWVudCJ9XX0.UIhCZHOx_3TgEy2eIIyUFbohuydqHlRV5tECCnLuNivsM44bhHNMUOm3XZZpS5OGXhMcrDeJsoNDirFGD8WRWA'
api_url_base = 'https://api.brawlstars.com/v1/'

headers = {'Content-Type': 'application/json',
            'Authorization': 'Bearer {0}'.format(api_token)}


def get_player(tag):
    api_url = '{0}players/{1}'.format(api_url_base, tag)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        print(response, api_url)
        return None

def get_battlelog(tag):
    api_url = '{0}players/{1}/battlelog'.format(api_url_base, tag)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        battles = json.loads(response.content.decode('utf-8'))
        return battles
    else:
        print(response, api_url)
        return None

def get_player_data(player):
    return player['name'], player['tag'], player['brawler']['name'], player['brawler']['power']

def get_battle_data(battle):
    data = {}
    battletime = battle['battleTime'].split('.')[0]
    data['datetime'] = [int(battletime.split('T')[0] + battletime.split('T')[1])]
    data['result'] = [battle['battle']['result']]
    data['mode'] = [battle['battle']['mode']]
    data['type'] = [battle['battle']['type']]
    data['duration'] = [battle['battle']['duration']]
    data['map'] = [battle['event']['map']]
    for i in range(2):
        for j in range(3):
            name, tag, brawler, power = get_player_data(battle['battle']['teams'][i][j]) 
            data['P{}{}_name'.format(i, j)] = [name]
            data['P{}{}_tag'.format(i, j)] = [tag]
            data['P{}{}_brawler'.format(i, j)] = [brawler]
            data['P{}{}_power'.format(i, j)] = [power]
    return pd.DataFrame(data)

def update_data(tag):
    file_name = 'log_{}.csv'.format(tag)
    df = None
    if os.path.isfile(file_name):
       df = pd.read_csv(file_name, sep=';')
    res = get_battlelog(tag)
    added = 0
    for r in res['items']:
        if(r['event']['mode'] not in ['soloShowdown', 'duoShowdown']):
            data = get_battle_data(r)
            if(df is None):
                df = data
                added += 1
            elif(data.datetime[0] not in df.datetime.values):
                df = df.append(data)
                added += 1

    print(tag, added, 'added')
    if(df is not None):
        df.to_csv(file_name, sep=';', index = False)

def run():
    while(True):
        for tag in tags:
            update_data(tag)
        time.sleep(600)

#run()