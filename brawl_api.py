import json
import requests
from collections import namedtuple
from pandas.io.json import json_normalize

class BrawlApi:
    def __init__(self, api_token):
        self.api_url_base = 'https://api.brawlstars.com/v1/'
        self.api_token = api_token
        self.headers = {'Content-Type': 'application/json',
            'Authorization': 'Bearer {0}'.format(self.api_token)}

    def get(self, url):
        return self.get_dict(url)

    def get_named(self, url):
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'), object_hook=lambda d: namedtuple('X', ['v'+k if k[0].isdigit() else k for k in d.keys()])(*d.values()))
        else:
            print(response, url)

    def get_dict(self, url):
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
        else:
            print(response, url)

    def get_dataframe(self, url):#bugado
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            return json_normalize(json.loads(response.content.decode('utf-8'))['items'])
        else:
            print(response, url)

    def get_player(self, tag):
        api_url = '{0}players/{1}'.format(self.api_url_base, tag.replace('#', '%23'))
        return self.get(api_url)
    
    def get_battlelog(self, tag):
        api_url = '{0}players/{1}/battlelog'.format(self.api_url_base, tag.replace('#', '%23'))
        return self.get(api_url)

    def get_club(self, tag):
        api_url = '{0}clubs/{1}'.format(self.api_url_base, tag.replace('#', '%23'))
        return self.get(api_url)

    def get_members(self, tag):
        api_url = '{0}clubs/{1}/members'.format(self.api_url_base, tag.replace('#', '%23'))
        return self.get(api_url)