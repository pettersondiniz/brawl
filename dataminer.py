import pandas as pd
import numpy as np
import json
import google_sheet as gs
id_sheet = '1CHMuBUqZj9B0Cs7jp3n1cNjpxhbL3jrbQLfm_YSr4co'
id_sheet_dzor = '1i1XZqsAbiqeeJxM244QaGz0cPq6RR-ZUxlEtcwLjJ-E'
id_sheet_luciano = '1q4LkLMGLpgn0q0UaCky5chfp1UQS9hUuVm8V0bUHp_s'
id_sheet_storm = '1ykZQ7jL_obTc0TVdD9ONfHTfoSTSbPfCBqYcWwn930c'

file_names = ['dzor_log_#PCL90CQQ.csv', 'dzor_log_#PQ2LJ0GPU.csv', 'Storm_log_#80Q29L2U.csv', 'Drilling_log_#20P2U8JLL.csv', 'Luciano_log_#20GVQ0UL.csv', 'Luffy_log_#29GC9RU0C.csv']

def read_file(file_name):
    df = pd.read_csv(file_name, sep=';', decimal=',')
    df['mode_map'] = df['mode'] + ' ' + df['map']
    df['others'] = df['p1'] + ' ' + df['p2']
    df = df.rename(columns={df.columns[0] : 'brawler'})
    return df

def get_data(df):
    print('')
    df['comp'] = df.brawler + ' ' + df.p1 + ' ' + df.p2
    
    print(df.result.value_counts())
    print(df.result.value_counts(normalize=True))

    print(df.brawler.value_counts())
    print(df.brawler.value_counts(normalize=True))

    print(df['mode'].value_counts())
    print(df['mode'].value_counts(normalize=True))

    print(df.comp.value_counts())
    print(df.comp.value_counts(normalize=True))

def get_statistics_comp(df):
    output = []
    comps = df.groupby(['brawler', 'p1', 'p2'])
    for comp,battles in comps:
        output.append([comp[0], comp[1], comp[2], np.sum(battles.result.values == 'victory'), np.sum(battles.result.values == 'defeat')])
    return output

def get_statistics_brawler(df):
    output = []
    brawlers = df.groupby(['brawler'])
    for brawler,battles in brawlers:
        output.append([brawler, np.sum(battles.result.values == 'victory'), np.sum(battles.result.values == 'defeat')])
    return output

def get_best_comps(df1, df2, df3):
    modes1 = df1.groupby('mode')
    modes2 = df2.groupby('mode')
    modes3 = df3.groupby('mode')

    comps_modes1 = {}
    comps_modes2 = {}
    comps_modes3 = {}

    for mode,battles in modes1:
        comps1 = get_statistics_comp(battles)
        brawlers1 = get_statistics_brawler(battles)
        comps_modes1[mode] = (comps1, brawlers1)

    for mode,battles in modes2:
        comps2 = get_statistics_comp(battles)
        brawlers2 = get_statistics_brawler(battles)
        comps_modes2[mode] = (comps2, brawlers2)
    
    for mode,battles in modes3:
        comps3 = get_statistics_comp(battles)
        brawlers3 = get_statistics_brawler(battles)
        comps_modes3[mode] = (comps3, brawlers3)

    return

def filter_df(df, field, value):
    return df[df[field] == value]

def get_values(df, field):
    return df[field].unique()

def get_other_brawler_usage(df):
    brawler = {}
    for v in get_values(df, 'brawler'):
        dff = filter_df(df, 'brawler', v)
        occ1 = get_occurrences(dff, 'p1')
        occ2 = get_occurrences(dff, 'p2')
        
        occ = pd.merge(occ1, occ2, how='outer', on=['field'])#occ1.join(occ2,on='field',  ignore_index=True)
        occ = occ.fillna(0)
        occ['count'] = occ['count_x'] + occ['count_y']
        # occ['rate'] = occ['count']/occ['count'].sum()
        #occ = occ.drop(['count_x','count_y','rate_x','rate_y'], axis=1)
        occ = occ.drop(['count_x','count_y'], axis=1)
        occ = occ.rename(columns={'field' : 'brawler'})
        occ = occ.sort_values(by=['count'], ascending=False)
        occ = occ.set_index('brawler').to_dict('index')
        brawler[v] = occ
    return brawler

def get_comp_usage(df):
    brawler = {}
    for v in get_values(df, 'brawler'):
        dff = filter_df(df, 'brawler', v)
        occ = get_occurrences(dff, 'others').rename(columns={'field' : 'brawlers'}).set_index('brawlers').to_dict('index')
        brawler[v] = occ
    return brawler

def get_occurrences(df, field, order = 'count'):
    values = get_values(df,field)
    arr = df[field].values
    arr = [[value, np.count_nonzero(arr == value)] for value in values]
    return pd.DataFrame(arr, columns=['field', 'count']).sort_values(by=[order], ascending=False)
    # arr = [[value, np.count_nonzero(arr == value), np.count_nonzero(arr == value)/len(arr)] for value in values]
    # return pd.DataFrame(arr, columns=['field', 'count', 'rate']).sort_values(by=[order], ascending=False)

def get_use(df):
    dff = df
    use = get_occurrences(dff, 'brawler').rename(columns={'field' : 'brawler'}).set_index('brawler').to_dict('index')
    for brawler in use:
        b = get_other_brawler_usage(dff)[brawler]
        for x in b:
            b[x] = b[x]['count']
        use[brawler].update(b)
        b = get_comp_usage(dff)[brawler]
        for x in b:
            b[x] = b[x]['count']
        use[brawler].update(b)
    return use

def get_use_by_filter(df, field):
    use = {}
    for v in get_values(df, field):
        dff = filter_df(df, field, v)
        use[v] = get_use(dff)
    return use

def get_brawler_info_by_result(df):
    return {'victory': get_brawler_info(filter_df(df, 'result', 'victory')), 'defeat': get_brawler_info(filter_df(df, 'result', 'defeat'))}

def get_brawler_info(df):
    general_use = get_use(df)
    mode_use = get_use_by_filter(df, 'mode')
    map_use = get_use_by_filter(df, 'mode_map')
    
    return {'general': general_use, 'mode': mode_use, 'map':map_use}

def save_dict_file(name, data):
    text_file = open(name + '.json', "w")
    text_file.write(json.dumps(data))
    text_file.close()

def dict_to_df(data):
    victory = data['victory']
    defeat = data['defeat']
    general_v = []
    for i in victory['general']:
        for j in victory['general'][i]:
            data = [i, j.replace('count', ''), victory['general'][i][j]]
            general_v.append(data)
    general_v = pd.DataFrame(general_v, columns=['brawler', 'others_brawlers', 'victory'])

    general_d = []
    for i in defeat['general']:
        for j in defeat['general'][i]:
            data = [i, j.replace('count', ''), defeat['general'][i][j]]
            general_d.append(data)
    general_d = pd.DataFrame(general_d, columns=['brawler', 'others_brawlers', 'defeat'])
    
    general = pd.merge(general_v, general_d, how='outer', on=['brawler', 'others_brawlers',])
    general = general.fillna(0)

    mode_v = []
    for i in victory['mode']:
        for j in victory['mode'][i]:
            for k in victory['mode'][i][j]:
                data = [i, j, k.replace('count', ''), victory['mode'][i][j][k]]
                mode_v.append(data)
    mode_v = pd.DataFrame(mode_v, columns=['brawler', 'mode', 'others_brawlers', 'victory'])

    mode_d = []
    for i in defeat['mode']:
        for j in defeat['mode'][i]:
            for k in defeat['mode'][i][j]:
                data = [i, j, k.replace('count', ''), defeat['mode'][i][j][k]]
                mode_d.append(data)
    mode_d = pd.DataFrame(mode_d, columns=['brawler', 'mode', 'others_brawlers', 'defeat'])
    
    mode = pd.merge(mode_v, mode_d, how='outer', on=['brawler', 'mode', 'others_brawlers',])
    
    mode = mode.fillna(0)

    map_v = []
    for i in victory['map']:
        for j in victory['map'][i]:
            for k in victory['map'][i][j]:
                data = [i, j, k.replace('count', ''), victory['map'][i][j][k]]
                map_v.append(data)
    map_v = pd.DataFrame(map_v, columns=['brawler', 'map', 'others_brawlers', 'victory'])

    map_d = []
    for i in defeat['map']:
        for j in defeat['map'][i]:
            for k in defeat['map'][i][j]:
                data = [i, j, k.replace('count', ''), defeat['map'][i][j][k]]
                map_d.append(data)
    map_d = pd.DataFrame(map_d, columns=['brawler', 'map', 'others_brawlers', 'defeat'])
    
    map = pd.merge(map_v, map_d, how='outer', on=['brawler', 'map', 'others_brawlers',])
    map = map.fillna(0)

    return general, mode, map

def run():
    df_storm = pd.concat([read_file(file_names[2]),read_file(file_names[3])], ignore_index=True)
    storm = get_brawler_info_by_result(df_storm)
    save_dict_file('storm', storm) 
    df_storm.to_csv('storm.csv', sep=';', decimal=',')
    gs.set_values('storm!A2:J', df_storm.drop(columns=['mode_map', 'others']), id_sheet)
    g, mo, ma = dict_to_df(storm)

    g.to_csv('stormg.csv', sep=';', decimal=',')
    mo.to_csv('stormmo.csv', sep=';', decimal=',')
    ma.to_csv('stormma.csv', sep=';', decimal=',')

    gs.set_values('storm!L2:O', g, id_sheet)
    gs.set_values('storm!Q2:U', mo, id_sheet)
    gs.set_values('storm!W2:AA', ma, id_sheet)

    gs.set_values('BattleLog!A2:J', df_storm.drop(columns=['mode_map', 'others']), id_sheet_storm)
    gs.set_values('GeneralStats!A2:D', g, id_sheet_storm)
    gs.set_values('ModeStats!A2:E', mo, id_sheet_storm)
    gs.set_values('MapStats!A2:E', ma, id_sheet_storm)

    df_luciano = pd.concat([read_file(file_names[4]),read_file(file_names[5])], ignore_index=True)
    luciano = get_brawler_info_by_result(df_luciano)
    save_dict_file('luciano', luciano) 
    df_luciano.to_csv('luciano.csv', sep=';', decimal=',')
    gs.set_values('luciano!A2:J', df_luciano.drop(columns=['mode_map', 'others']), id_sheet)
    g, mo, ma = dict_to_df(luciano)
    g.to_csv('lucianog.csv', sep=';', decimal=',')
    mo.to_csv('lucianomo.csv', sep=';', decimal=',')
    ma.to_csv('lucianoma.csv', sep=';', decimal=',')

    gs.set_values('luciano!L2:O', g, id_sheet)
    gs.set_values('luciano!Q2:U', mo, id_sheet)
    gs.set_values('luciano!W2:AA', ma, id_sheet)

    gs.set_values('BattleLog!A2:J', df_luciano.drop(columns=['mode_map', 'others']), id_sheet_luciano)
    gs.set_values('GeneralStats!A2:D', g, id_sheet_luciano)
    gs.set_values('ModeStats!A2:E', mo, id_sheet_luciano)
    gs.set_values('MapStats!A2:E', ma, id_sheet_luciano)
    
    df_dzor = pd.concat([read_file(file_names[0]),read_file(file_names[1])], ignore_index=True)
    dzor = get_brawler_info_by_result(df_dzor)
    save_dict_file('dzor', dzor)
    df_dzor.to_csv('dzor.csv', sep=';', decimal=',')
    gs.set_values('dzor!A2:J', df_dzor.drop(columns=['mode_map', 'others']), id_sheet)
    g, mo, ma = dict_to_df(dzor)
    g.to_csv('dzorg.csv', sep=';', decimal=',')
    mo.to_csv('dzormo.csv', sep=';', decimal=',')
    ma.to_csv('dzorma.csv', sep=';', decimal=',')
    
    gs.set_values('dzor!L2:O', g, id_sheet)
    gs.set_values('dzor!Q2:U', mo, id_sheet)
    gs.set_values('dzor!W2:AA', ma, id_sheet)

    gs.set_values('BattleLog!A2:J', df_dzor.drop(columns=['mode_map', 'others']), id_sheet_dzor)
    gs.set_values('GeneralStats!A2:D', g, id_sheet_dzor)
    gs.set_values('ModeStats!A2:E', mo, id_sheet_dzor)
    gs.set_values('MapStats!A2:E', ma, id_sheet_dzor)

