
import pandas as pd
import numpy as np
import bralw
import time
import os
import dataminer as dm
import google_sheet as gs

# import google_serv
# CLIENT_SECRET_FILE = 'client_secret.json'
# API_SERVICE_NAME = 'sheets'
# API_VERSION = 'v4'
# SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
# gsheetId = '1i1XZqsAbiqeeJxM244QaGz0cPq6RR-ZUxlEtcwLjJ-E'

# service = google_serv.Create_Service(CLIENT_SECRET_FILE, API_SERVICE_NAME, API_VERSION, SCOPES)

# def export_data_to_sheets(df):
#     response_date = service.spreadsheets().values().append(
#         spreadsheetId=gsheetId,
#         valueInputOption='RAW',
#         range='BattleLog!A1',
#         body=dict(
#             majorDimension='ROWS',
#             values=df.T.reset_index().T.values.tolist())
#     ).execute()
id_sheet = '1i1XZqsAbiqeeJxM244QaGz0cPq6RR-ZUxlEtcwLjJ-E'

file_names = ['log_#PCL90CQQ.csv', 'log_#PQ2LJ0GPU.csv']#['log_%23PQ2LJ0GPU.csv','log_%23PCL90CQQ.csv']

tags = ['#PCL90CQQ', '#PQ2LJ0GPU']
file_name = 'log_#PCL90CQQ.csv'
player_name ='dzor'

def process_battles(battles):
    data = {}
    data['{}_brawler'.format(player_name)] = []
    data['p1'] = []
    data['p2'] = []
    data['result'] = []
    data['mode'] = []
    data['map'] = []
    data['duration'] = []
    data['ad1_brawler'] = []
    data['ad2_brawler'] = []
    data['ad3_brawler'] = []
    for _, row in battles.iterrows():
        if (row['P00_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P00_brawler'])
            data['p1'].append(min(row['P01_brawler'], row['P02_brawler']))
            data['p2'].append(max(row['P01_brawler'], row['P02_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P01_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P01_brawler'])
            data['p1'].append(min(row['P00_brawler'], row['P02_brawler']))
            data['p2'].append(max(row['P00_brawler'], row['P02_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P02_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P02_brawler'])
            data['p1'].append(min(row['P01_brawler'], row['P00_brawler']))
            data['p2'].append(max(row['P01_brawler'], row['P00_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        if (row['P10_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P10_brawler'])
            data['p1'].append(min(row['P11_brawler'], row['P12_brawler']))
            data['p2'].append(max(row['P11_brawler'], row['P12_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P11_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P11_brawler'])
            data['p1'].append(min(row['P10_brawler'], row['P12_brawler']))
            data['p2'].append(max(row['P10_brawler'], row['P12_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P12_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P12_brawler'])
            data['p1'].append(min(row['P11_brawler'], row['P10_brawler']))
            data['p2'].append(max(row['P11_brawler'], row['P10_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        data['mode'].append(row['mode'])
        data['result'].append(row['result'])
        data['map'].append(row['map'])
        data['duration'].append(row['duration'])

    return pd.DataFrame(data)

def get_statistics(battles):
    print('General Stats')
    print('Win Rate')
    print(battles.result.value_counts())
    print(battles.result.value_counts(normalize=True))
    print('Comps')
    brawlers_cols = ['{}_brawler'.format(player_name)]#, 'p1', 'p2']
    print(battles.groupby(brawlers_cols).size())
    print('Result')
    print(battles.groupby(['result']+brawlers_cols).size())
    print('Mode')
    print(battles.groupby(['mode']+brawlers_cols).size())
    print('Mode/Map')
    print(battles.groupby(['mode','map']+brawlers_cols).size())
    print('Result/Mode')
    print(battles.groupby(['result','mode']+brawlers_cols).size())
    print('Result/Mode/map')
    print(battles.groupby(['result','mode','map']+brawlers_cols).size())

def run():
    #print(pd.read_csv("https://bitbucket.org/pettersondiniz/brawl/src/master/personal_log_%2523PCL90CQQ.csv"))
    full_df = None
    for tag in tags:
        file_name = 'log_{}.csv'.format(tag)
        df = pd.read_csv(file_name, sep=';')
        df = process_battles(df)
        #get_statistics(df)
        if(full_df is None):
            full_df = df.copy()
        else:
            full_df = pd.concat([full_df,df], ignore_index=True)
        df.to_csv('personal_{}'.format(file_name), sep=';', index=False)
        #export_data_to_sheets(df)
    full_df.to_csv('personal_full.csv', sep=';', index=False)
    gs.set_values('BattleLog!A2:J', full_df, id_sheet)

# for file_name in file_names:
#     df = pd.read_csv(file_name, sep=';')
#     df = process_battles(df)
#     get_statistics(df)
#     print(df)
#     df.to_csv('personal_{}'.format(file_name), sep=';', index=False)
#     export_data_to_sheets(df)
