import pandas as pd
import numpy as np
import dataminer as dm
from joblib import Parallel, delayed
import multiprocessing


file_names = dm.file_names


def get_estimated_win_rate_comp(df, brawler, p1, p2):
    filtered_brawler = df[(df['brawler'] == brawler)]
    filtered_semi = df[(df['brawler'] == brawler) & ((df['p1'] == p1) | (df['p1'] == p2) | (df['p2'] == p1) | (df['p2'] == p2)) ]
    filtered_comp = df[(df['brawler'] == brawler) & ((df['p1'] == p1) & (df['p2'] == p2) | (df['p2'] == p1) & (df['p1'] == p2)) ]
    df_general = pd.concat([filtered_brawler, filtered_semi, filtered_comp], ignore_index=True)
    count = float(df_general.shape[0])
    array = df_general['result'].values
    winrate = 0 if count == 0 else float(np.count_nonzero(array == 'victory'))/count
    return count, winrate


def get_suggested_comps(df_dzor, df_storm, df_luciano):
    all_brawlers = [
        #region brawllers
        'POCO',
        'JESSIE',
        'EL PRIMO',
        'ROSA',
        'NITA',
        '8-BIT',
        'PIPER',
        'BULL',
        'BARLEY',
        'PENNY',
        'SPROUT',
        'BEA',
        'EMZ',
        'BROCK',
        'GENE',
        'MORTIS',
        'TICK',
        'SPIKE',
        'PAM',
        'JACKY',
        'COLT',
        'BO',
        'FRANK',
        'MAX',
        'DYNAMIKE',
        'SANDY',
        'RICO',
        'TARA',
        'CARL',
        'BIBI',
        'CROW',
        'SHELLY',
        'DARRYL',
        'MR. P',
        'LEON',
        'GALE'
        #endregion
    ]
    all_comps = []
    total = len(all_brawlers)
    total = total*total*total
    i = 1
    for dzor_brawler in all_brawlers:
        for storm_brawler in all_brawlers:
            for luciano_brawler in all_brawlers:
                if(not ((dzor_brawler == luciano_brawler) | (luciano_brawler == storm_brawler) | (storm_brawler == dzor_brawler))):
                    count_dzor, winrate_dzor = get_estimated_win_rate_comp(df_dzor, dzor_brawler, storm_brawler, luciano_brawler)
                    count_storm, winrate_storm = get_estimated_win_rate_comp(df_storm, storm_brawler, dzor_brawler, luciano_brawler)
                    count_luciano, winrate_luciano = get_estimated_win_rate_comp(df_luciano, luciano_brawler, dzor_brawler, storm_brawler)
                    
                    count = np.sum([count_dzor, count_storm, count_luciano])
                    average = 0 if count == 0 else np.average([winrate_dzor, winrate_storm, winrate_luciano], weights=[count_dzor, count_storm, count_luciano])
                    comp = [
                    dzor_brawler, storm_brawler, luciano_brawler, count_dzor, count_storm, count_luciano, winrate_dzor, winrate_storm, winrate_luciano,
                    count, 
                    average]
                    all_comps.append(comp)
                print('%s/%s %s%%' % (i, total, int((i/total)*100)))
                #print('\r%s/%s %s%%' % (i, total, int((i/total)*100)), end = '\r')
                i += 1
    return pd.DataFrame(all_comps, columns=['dzor', 'storm', 'luciano', 'count_dzor', 'count_storm', 'count_luciano', 'winrate_dzor', 'winrate_storm', 'winrate_luciano', 'count', 'winrate']).sort_values(by=['count', 'winrate'], ascending=False)

df_storm = pd.concat([dm.read_file(file_names[2]),dm.read_file(file_names[3])], ignore_index=True)
df_luciano = pd.concat([dm.read_file(file_names[4]),dm.read_file(file_names[5])], ignore_index=True)
df_dzor = pd.concat([dm.read_file(file_names[0]),dm.read_file(file_names[1])], ignore_index=True)

def process_mode(mode):
    df_dzor_f = dm.filter_df(df_dzor, 'mode', mode)
    df_luciano_f = dm.filter_df(df_luciano, 'mode', mode)
    df_storm_f = dm.filter_df(df_storm, 'mode', mode)

    comps_winrate = get_suggested_comps(df_dzor_f, df_storm_f, df_luciano_f)
    comps_winrate.to_csv('comps_winrate_{}.csv'.format(mode), sep=';', decimal=',')

num_cores = 6
results = Parallel(n_jobs=num_cores)(delayed(process_mode)(mode) for mode in ['gemGrab', 'brawlBall', 'heist', 'bounty', 'siege', 'hotZone'])
    