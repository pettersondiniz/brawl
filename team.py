import pandas as pd
import google_sheet as gs

file_name = 'log_#PQ2LJ0GPU.csv'
p0 = 'dzor'
p1 = 'Drilling'
p2 = 'Luffy'

id_sheet = '1CHMuBUqZj9B0Cs7jp3n1cNjpxhbL3jrbQLfm_YSr4co'

def filter_battles(battles):
    battles = battles[(battles.P00_name == p0) | (battles.P00_name == p1) | (battles.P00_name == p2) |
                      (battles.P10_name == p0) | (battles.P10_name == p1) | (battles.P10_name == p2) ]
    battles = battles[(battles.P01_name == p0) | (battles.P01_name == p1) | (battles.P01_name == p2) |
                      (battles.P11_name == p0) | (battles.P11_name == p1) | (battles.P11_name == p2)]
    battles = battles[(battles.P02_name == p0) | (battles.P02_name == p1) | (battles.P02_name == p2) | 
                      (battles.P12_name == p0) | (battles.P12_name == p1) | (battles.P12_name == p2)]
    return battles

def process_battles(battles):
    data = {}
    data['{}_brawler'.format(p0)] = []
    data['{}_brawler'.format(p1)] = []
    data['{}_brawler'.format(p2)] = []
    #data['{}_power'.format(p0)] = []
    #data['{}_power'.format(p1)] = []
    #data['{}_power'.format(p2)] = []
    data['result'] = []
    data['mode'] = []
    data['map'] = []
    data['duration'] = []
    data['ad1_brawler'] = []
    data['ad2_brawler'] = []
    data['ad3_brawler'] = []
    #data['ad1_power'] = []
    #data['ad2_power'] = []
    #data['ad3_power'] = []
    for _, row in battles.iterrows():
        data['{}_brawler'.format(row['P00_name'])].append(row['P00_brawler'])
        data['{}_brawler'.format(row['P01_name'])].append(row['P01_brawler'])
        data['{}_brawler'.format(row['P02_name'])].append(row['P02_brawler'])
        #data['{}_power'.format(row['P00_name'])].append(row['P00_power'])
        #data['{}_power'.format(row['P01_name'])].append(row['P01_power'])
        #data['{}_power'.format(row['P02_name'])].append(row['P02_power'])
        data['mode'].append(row['mode'])
        data['result'].append(row['result'])
        data['map'].append(row['map'])
        data['duration'].append(row['duration'])
        data['ad1_brawler'].append(row['P10_brawler'])
        data['ad2_brawler'].append(row['P11_brawler'])
        data['ad3_brawler'].append(row['P12_brawler'])
        #data['ad1_power'].append(row['P10_power'])
        #data['ad2_power'].append(row['P11_power'])
        #data['ad3_power'].append(row['P12_power'])
    
    return pd.DataFrame(data)

def fix_team(battles):
    out_df = pd.DataFrame(columns = battles.columns)
    for i, row in battles.iterrows():
        if p0 in [row['P10_name'], row['P11_name'], row['P12_name']]:
            for i in range(3):
                name = row['P0{}_name'.format(i)]
                brawler = row['P0{}_brawler'.format(i)]
                power = row['P0{}_power'.format(i)]
                _tag = row['P0{}_tag'.format(i)]
                row['P0{}_name'.format(i)] = row['P1{}_name'.format(i)]
                row['P0{}_brawler'.format(i)] = row['P1{}_brawler'.format(i)]
                row['P0{}_power'.format(i)] = row['P1{}_power'.format(i)]
                row['P0{}_tag'.format(i)] = row['P1{}_tag'.format(i)]
                row['P1{}_name'.format(i)] = name
                row['P1{}_brawler'.format(i)] = brawler
                row['P1{}_power'.format(i)] = power
                row['P1{}_tag'.format(i)] = _tag
        out_df = out_df.append(row, ignore_index=True)
    #print(out_df)
    return out_df


def get_statistics(battles):
    print('General Stats')
    print('Win Rate')
    print(battles.result.value_counts())
    print(battles.result.value_counts(normalize=True))
    print('Comps')
    brawlers_cols = ['{}_brawler'.format(p0), '{}_brawler'.format(p1), '{}_brawler'.format(p2)]
    print(battles.groupby(brawlers_cols).size())
    print('Result')
    print(battles.groupby(['result']+brawlers_cols).size())
    print('Mode')
    print(battles.groupby(['mode']+brawlers_cols).size())
    print('Mode/Map')
    print(battles.groupby(['mode','map']+brawlers_cols).size())
    print('Result/Mode')
    print(battles.groupby(['result','mode']+brawlers_cols).size())
    print('Result/Mode/map')
    print(battles.groupby(['result','mode','map']+brawlers_cols).size())



    #df.groupby(['key1', 'key2']).size()

def run():
    df = pd.read_csv(file_name, sep=';')
    df = filter_battles(df)
    df = fix_team(df)
    df = process_battles(df)
    df.to_csv('team.csv', sep=';', index=False)
    gs.set_values('BattleLog!A2:J', df, id_sheet)

    #get_statistics(df)