import pandas as pd
import numpy as np
import brawl_api
import team
import time
import os
import google_sheet as gs
import dataminer as dm
import personal

api_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjM4NTU5NjgwLWYxZjItNGFiYS05NjEzLTFjMzNlNjAzZjNjZCIsImlhdCI6MTU5MDYzNjg3Miwic3ViIjoiZGV2ZWxvcGVyL2EyNDYwMDRmLTExN2QtNTA4OS01YzNkLWY4OWI4MGM1YzU2MSIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTc5LjY3LjgzLjcyIl0sInR5cGUiOiJjbGllbnQifV19.QV2qDdM0a0k8Nq14c-WdwcE-FxOIaJvlpY2iaYBujltWB_aQjBAYzC6Ayfq_vmszESJZj3_5NXUpiNLkhdEQQg'
tags = ['#PCL90CQQ', '#20GVQ0UL', '#80Q29L2U', '#PQ2LJ0GPU', '#20P2U8JLL', '#29GC9RU0C']
api = brawl_api.BrawlApi(api_token)
#player_names = ['dzor', 'Luciano', 'Storm', 'dzor', 'Drilling', 'Luffy']
id_sheet = '1CHMuBUqZj9B0Cs7jp3n1cNjpxhbL3jrbQLfm_YSr4co'
def get_player_data(player):
    return player['name'], player['tag'], player['brawler']['name'], 0#player['brawler']['power']

def get_battle_data(battle):
    data = {}
    battletime = battle['battleTime'].split('.')[0]
    data['datetime'] = [int(battletime.split('T')[0] + battletime.split('T')[1])]
    data['result'] = [battle['battle']['result']]
    data['mode'] = [battle['battle']['mode']]
    data['type'] = [battle['battle']['type']]
    data['duration'] = [battle['battle']['duration']]
    data['map'] = [battle['event']['map']]
    for i in range(2):
        for j in range(3):
            name, tag, brawler, power = get_player_data(battle['battle']['teams'][i][j]) 
            data['P{}{}_name'.format(i, j)] = [name]
            data['P{}{}_tag'.format(i, j)] = [tag]
            data['P{}{}_brawler'.format(i, j)] = [brawler]
            data['P{}{}_power'.format(i, j)] = [power]
    return pd.DataFrame(data)

def update_data(tag, file_name):
    df = None
    if os.path.isfile(file_name):
        df = pd.read_csv(file_name, sep=';')
    res = api.get_battlelog(tag)
    added = 0
    for r in res['items']:
        if(r['event']['mode'] in ['gemGrab', 'brawlBall', 'heist', 'bounty', 'siege', 'hotZone']):# and (r['battle']['type'] != 'friendly'):
            data = get_battle_data(r)
            if(df is None):
                df = data
                added += 1
            elif(data.datetime[0] not in df.datetime.values):
                df = df.append(data)
                added += 1
    print(added, 'new 3vs3 battles')
    if(df is not None):
        df.to_csv(file_name, sep=';', index = False)

def process_battles(battles, player_name):
    data = {}
    data['{}_brawler'.format(player_name)] = []
    data['p1'] = []
    data['p2'] = []
    data['result'] = []
    data['mode'] = []
    data['map'] = []
    data['duration'] = []
    data['ad1_brawler'] = []
    data['ad2_brawler'] = []
    data['ad3_brawler'] = []
    for _, row in battles.iterrows():
        if (row['P00_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P00_brawler'])
            data['p1'].append(min(row['P01_brawler'], row['P02_brawler']))
            data['p2'].append(max(row['P01_brawler'], row['P02_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P01_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P01_brawler'])
            data['p1'].append(min(row['P00_brawler'], row['P02_brawler']))
            data['p2'].append(max(row['P00_brawler'], row['P02_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P02_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P02_brawler'])
            data['p1'].append(min(row['P01_brawler'], row['P00_brawler']))
            data['p2'].append(max(row['P01_brawler'], row['P00_brawler']))
            ads = [row['P10_brawler'], row['P11_brawler'], row['P12_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        if (row['P10_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P10_brawler'])
            data['p1'].append(min(row['P11_brawler'], row['P12_brawler']))
            data['p2'].append(max(row['P11_brawler'], row['P12_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P11_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P11_brawler'])
            data['p1'].append(min(row['P10_brawler'], row['P12_brawler']))
            data['p2'].append(max(row['P10_brawler'], row['P12_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        elif (row['P12_name'] == player_name):
            data['{}_brawler'.format(player_name)].append(row['P12_brawler'])
            data['p1'].append(min(row['P11_brawler'], row['P10_brawler']))
            data['p2'].append(max(row['P11_brawler'], row['P10_brawler']))
            ads = [row['P00_brawler'], row['P01_brawler'], row['P02_brawler']]
            list.sort(ads)
            data['ad1_brawler'].append(ads[0])
            data['ad2_brawler'].append(ads[1])
            data['ad3_brawler'].append(ads[2])
        data['mode'].append(row['mode'])
        data['result'].append(row['result'])
        data['map'].append(row['map'])
        data['duration'].append(row['duration'])

    return pd.DataFrame(data)

while True:
    for tag in tags:
        player = api.get_player(tag)
        name = player['name']
        print(name)
        file_name = 'log_{}.csv'.format(tag)
        update_data(tag, file_name)
        if os.path.isfile(file_name):
            df = pd.read_csv(file_name, sep=';')
            df = process_battles(df, name)
            df.to_csv('{}_{}'.format(name, file_name), sep=';', index=False)
    team.run()
    dm.run()
    #personal.run()
    print('wait 10 min')
    time.sleep(600)