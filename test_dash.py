import plotly.express as px
import pandas as pd
import numpy as np

df = pd.read_csv('dzor_log_#PCL90CQQ.csv', sep=';')

labels = df['dzor_brawler'].value_counts().index
values = df['dzor_brawler'].value_counts().values
fig = px.pie(df, values=values, names=labels) 

import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()
app.layout = html.Div([
    dcc.Graph(
        id="brawler_usage",
        figure=fig)
])

app.run_server(debug=True)  # Turn off reloader if inside Jupyter